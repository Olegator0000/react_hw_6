import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import FavoriteButton from '../button/FavoriteButton';

test('FavoriteButton calls onClick when clicked', () => {
    const handleClick = jest.fn();
    const { getByRole } = render(<FavoriteButton onClick={handleClick} isFavorite={false} />);

    fireEvent.click(getByRole('button'));

    expect(handleClick).toHaveBeenCalledTimes(1);
});
