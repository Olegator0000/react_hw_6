import React, { useContext } from 'react';
import { ViewModeContext } from './ViewModeContext';

const ViewModeSwitcher = () => {
    const { viewMode, setViewMode } = useContext(ViewModeContext);

    return (
        <div className="view-mode-switcher">
            <button onClick={() => setViewMode('grid')} disabled={viewMode === 'grid'}>
                Grid View
            </button>
            <button onClick={() => setViewMode('list')} disabled={viewMode === 'list'}>
                List View
            </button>
        </div>
    );
};

export default ViewModeSwitcher;
