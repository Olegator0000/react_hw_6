import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import BuyButton from '../button/BuyButton';

test('BuyButton calls onClick when clicked', () => {
    const handleClick = jest.fn();
    const { getByText } = render(<BuyButton onClick={handleClick} />);

    fireEvent.click(getByText(/Buy/i));

    expect(handleClick).toHaveBeenCalledTimes(1);
});
