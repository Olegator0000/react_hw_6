// Home.test.js
import React from 'react';
import { render } from '@testing-library/react';
import Home from '../components/Home';

describe('Снепшот-тести', () => {
    test('Компонент Home відповідає снепшоту', () => {
        const { container } = render(<Home />);
        expect(container).toMatchSnapshot();
    });
});
