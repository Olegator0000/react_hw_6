import favoritesReducer, { addToFavorites, removeFromFavorites } from '../slices/favoritesSlice';

describe('favoritesReducer', () => {
    const initialState = { items: [] };

    test('should handle addToFavorites', () => {
        const newItem = { id: 1, name: 'Product 1', price: 100 };
        const action = addToFavorites(newItem);
        const newState = favoritesReducer(initialState, action);

        expect(newState.items).toHaveLength(1);
        expect(newState.items[0]).toEqual(newItem);
    });

    test('should handle removeFromFavorites', () => {
        const state = { items: [{ id: 1, name: 'Product 1', price: 100 }] };
        const action = removeFromFavorites(1);
        const newState = favoritesReducer(state, action);

        expect(newState.items).toHaveLength(0);
    });
});
