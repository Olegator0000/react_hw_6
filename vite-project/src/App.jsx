import React from 'react';
import { Route, Routes } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import Home from './components/Home';
import CartPage from './components/CartPage';
import FavoritesPage from './components/FavoritesPage';
import Header from './components/header/header';
import Navbar from './components/Navbar';
import { ViewModeProvider } from './components/ViewModeContext'; // Імпорт контексту
import './App.css';
import { addToCart, removeFromCart } from './redux/slices/cartSlice';
import { addToFavorites, removeFromFavorites } from './redux/slices/favoritesSlice';
import { openModal, closeModal } from './redux/slices/modalSlice';

function App() {
    const cart = useSelector((state) => state.cart);
    const favorites = useSelector((state) => state.favorites);
    const dispatch = useDispatch();

    const handleAddToCart = (item) => {
        dispatch(addToCart(item));
    };

    const handleRemoveFromCart = (itemId) => {
        dispatch(removeFromCart(itemId));
    };

    const handleAddToFavorites = (item) => {
        dispatch(addToFavorites(item));
    };

    const handleRemoveFromFavorites = (itemId) => {
        dispatch(removeFromFavorites(itemId));
    };

    return (
        <div>
            <Header cartCount={cart.length} favoriteCount={favorites.length} />
            <Navbar />
            <Routes>
                <Route
                    path="/"
                    element={<Home addToCart={handleAddToCart} addToFavorites={handleAddToFavorites} />}
                />
                <Route
                    path="/cart"
                    element={<CartPage cart={cart} removeFromCart={handleRemoveFromCart} />}
                />
                <Route
                    path="/favorites"
                    element={
                        <FavoritesPage
                            favorites={favorites} removeFromFavorites={handleRemoveFromFavorites} />}
                />
            </Routes>
        </div>
    );
}

export default App;
