import cartReducer, { addToCart, removeFromCart } from '../slices/cartSlice';

describe('cartReducer', () => {
    const initialState = { items: [] };

    test('should handle addToCart', () => {
        const newItem = { id: 1, name: 'Product 1', price: 100 };
        const action = addToCart(newItem);
        const newState = cartReducer(initialState, action);

        expect(newState.items).toHaveLength(1);
        expect(newState.items[0]).toEqual(newItem);
    });

    test('should handle removeFromCart', () => {
        const state = { items: [{ id: 1, name: 'Product 1', price: 100 }] };
        const action = removeFromCart(1);
        const newState = cartReducer(state, action);

        expect(newState.items).toHaveLength(0);
    });
});
