
import React from 'react';
import { useFormik } from 'formik';
import * as Yup from "yup";
import { useDispatch, useSelector } from 'react-redux';
import { clearCart } from '../redux/slices/cartSlice';

const CheckoutForm = () => {
    const dispatch = useDispatch();
    const cart = useSelector((state) => state.cart);

    const formik = useFormik({
        initialValues: {
            firstName: '',
            lastName: '',
            age: '',
            address: '',
            phone: '',
        },
        validationSchema: Yup.object({
            firstName: Yup.string().required('Required'),
            lastName: Yup.string().required('Required'),
            age: Yup.number().required('Required').positive('Must be positive').integer('Must be an integer'),
            address: Yup.string().required('Required'),
            phone: Yup.string().required('Required').matches(/^\+380\d{9}$/, 'not a number').min(10, 'Must be at least 10 numeric'),
        }),
        onSubmit: (values) => {
            console.log('Purchased items:', cart);
            console.log('User details:', values);
            dispatch(clearCart());
            localStorage.removeItem('cart');
        },
    });

    return (
        <form onSubmit={formik.handleSubmit}>
            <div>
                <label htmlFor="firstName">First Name</label>  <input
                    id="firstName"
                    name="firstName"
                    type="text"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.firstName}
                />
                {formik.touched.firstName && formik.errors.firstName ? (
                    <div>{formik.errors.firstName}</div>
                ) : null}
            </div>
            <div>
                <label htmlFor="lastName">Last Name</label>  <input
                    id="lastName"
                    name="lastName"
                    type="text"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.lastName}
                />
                {formik.touched.lastName && formik.errors.lastName ? (
                    <div>{formik.errors.lastName}</div>
                ) : null}
            </div>
            <div>
                <label htmlFor="age">Age</label>  <input
                    id="age"
                    name="age"
                    type="number"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.age}
                />
                {formik.touched.age && formik.errors.age ? (
                    <div>{formik.errors.age}</div>
                ) : null}
            </div>
            <div>
                <label htmlFor="address">Address</label>  <input
                    id="address"
                    name="address"
                    type="text"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.address}
                />
                {formik.touched.address && formik.errors.address ? (
                    <div>{formik.errors.address}</div>
                ) : null}
            </div>
            <div>
                <label htmlFor="phone">Phone</label>  <input
                    id="phone"
                    name="phone"
                    type="text"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.phone}
                />
                {formik.touched.phone && formik.errors.phone ? (
                    <div>{formik.errors.phone}</div>
                ) : null}
            </div>
            <button type="submit">Checkout</button>
        </form>
    );
};

export default CheckoutForm;
