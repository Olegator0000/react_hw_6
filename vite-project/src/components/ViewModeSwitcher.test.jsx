import React from 'react';
import renderer from 'react-test-renderer';
import ViewModeSwitcher from '../ViewModeSwitcher';
import { ViewModeProvider } from '../ViewModeContext';

test('ViewModeSwitcher renders correctly', () => {
    const tree = renderer
        .create(
            <ViewModeProvider>
                <ViewModeSwitcher />
            </ViewModeProvider>
        )
        .toJSON();
    expect(tree).toMatchSnapshot();
});
