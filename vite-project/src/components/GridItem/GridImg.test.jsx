import React from 'react';
import renderer from 'react-test-renderer';
import GridImg from '../GridItem/GridImg';

test('GridImg renders correctly', () => {
    const tree = renderer
        .create(
            <GridImg
                items={[{ id: 1, name: 'Product 1', price: 100, imagePath: '/img1.jpg', alt: 'Product 1', sku: 'sku1', color: 'red' }]}
                handleBuyButtonClick={jest.fn()}
                handleFavoriteButtonClick={jest.fn()}
                favorites={[]}
                viewMode="grid"
            />
        )
        .toJSON();
    expect(tree).toMatchSnapshot();
});
