import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import ModalWindow from '../ModalWindow';

test('ModalWindow calls onClose when close button is clicked', () => {
    const handleClose = jest.fn();
    const { getByText } = render(
        <ModalWindow isOpen={true} onClose={handleClose}>
            <p>Modal Content</p>
        </ModalWindow>
    );

    fireEvent.click(getByText(/Close/i));

    expect(handleClose).toHaveBeenCalledTimes(1);
});
