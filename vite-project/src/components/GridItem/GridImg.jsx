import React, { useContext } from 'react';
import BuyButton from '../button/BuyButton';
import FavoriteButton from '../button/FavoriteButton';
import PropTypes from 'prop-types';
import { ViewModeContext } from '../ViewModeContext';
import './Grid.scss';
import './List.scss';

const GridImg = ({ items, handleFavoriteButtonClick, handleBuyButtonClick, favorites }) => {
    const { viewMode } = useContext(ViewModeContext);

    return (
        <div className={viewMode === 'grid' ? 'grid' : 'list'}>
            {items.map((item) => (
                <div key={item.id} className={viewMode === 'grid' ? 'grid-item' : 'list-item'}>
                    <img src={item.imagePath} alt={item.alt} />
                    <div className="details">
                        <h2>Name: {item.name}</h2>
                        <p>Price: ${item.price}</p>
                        <p>SKU: {item.sku}</p>
                        <p>Color: {item.color}</p>
                        <BuyButton onClick={() => handleBuyButtonClick(item)} />
                        <FavoriteButton
                            onClick={() => handleFavoriteButtonClick(item)}
                            isFavorite={favorites.some(fav => fav.id === item.id)}
                        />
                    </div>
                </div>
            ))}
        </div>
    );
};

GridImg.propTypes = {
    items: PropTypes.arrayOf(PropTypes.object).isRequired,
    handleFavoriteButtonClick: PropTypes.func.isRequired,
    handleBuyButtonClick: PropTypes.func.isRequired,
    favorites: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default GridImg;
